﻿## Interface: 70000
## Title: CandyBuckets
## Notes: Shows map icons for Lunar Festival, Hallow's End and Midsummer Fire Festival!
## Author: Vladinator
## Version: 7.0.0.160629

libs\LibStub\LibStub.lua
libs\CallbackHandler-1.0\CallbackHandler-1.0.lua
libs\HereBeDragons-1.0\HereBeDragons-1.0.lua
libs\HereBeDragons-1.0\HereBeDragons-Pins-1.0.lua

ns.lua

db\hallow.lua
db\lunar.lua
db\midsummer.lua

core.lua
